# pandoc -t beamer --slide-level 2 -o out.pdf in.md
#
all:	HOWTO-x230-to-nitropad-slides.pdf HOWTO-x230-to-nitropad.pdf

# generate slides
HOWTO-x230-to-nitropad-slides.pdf:	HOWTO-TURN-X230-INTO-NITROPAD.rst
	pandoc -t beamer -f rst --slide-level 2 -o $@ $^

# generate a PDF article
HOWTO-x230-to-nitropad.pdf:	HOWTO-TURN-X230-INTO-NITROPAD.rst
	pandoc -t latex -f rst --slide-level 2 -o $@ $^
