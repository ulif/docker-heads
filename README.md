# docker-heads

Build heads images for Lenovo x230


*Build:*

    docker build -t heads .


*Run:*

    docker run --rm -it -v heads:/root/heads heads bash

*Get results*

    docker run --rm -it -v `pwd`/heads-build:/data heads cp -ar /root/heads /data

