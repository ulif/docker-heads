# Build heads for Lenovo x230
FROM debian:9
MAINTAINER ulif

#
# Step 3
#
# According to https://github.com/hamishcoleman/thinkpad-ec
# unlock whitelisting
RUN apt update \
  && apt install -y -q --no-install-recommends \
  build-essential git mtools libssl-dev ca-certificates wget \
  zlib1g-dev uuid-dev libdigest-sha-perl libelf-dev bc \
  bzip2 bison flex gnupg iasl  m4 nasm  openssl patch \
  python gnat cpio ccache pkg-config cmake libusb-1.0-0-dev \
  texinfo \
 && apt-get clean \
 && rm -r /var/lib/apt/lists/*
RUN mkdir -p /build/results

RUN git clone https://github.com/hamishcoleman/thinkpad-ec /build/thinkpad-ec

WORKDIR /build/thinkpad-ec

RUN cp defconfig .config \
  && make list_laptops \
  && make patch_enable_battery clean \
  && make patched.x230.img \
  && cp patched.x230* /build/results


#
# Step 5 - coreboot (with skulls)
#
RUN git clone https://review.coreboot.org/coreboot /build/coreboot

WORKDIR /build/coreboot

RUN git submodule update --init --checkout
RUN make crossgcc-i386 CPUS=$(nproc)
RUN make crossgcc-x64 CPUS=$(nproc)
RUN cd util/ifdtool && make
RUN cd util/nvramtool && make


#
# Step 8: heads
#
# According to http://osresearch.net/Building
RUN git clone https://github.com/osresearch/heads /build/heads

WORKDIR /build/heads

RUN make
RUN sed -i '/^CONFIG_FBWHIPTAIL=.*/a CONFIG_LIBREMKEY=y' boards/x230/x230.config
RUN make BOARD=x230
RUN cp -r /build/heads/build/x230 /build/results/heads-x230




# RUN git clone https://github.com/corna/me_cleaner.git

# RUN make && make BOARD=x230 && make BOARD=x230-flash

# RUN mkdir /data

# VOLUME ["/build/heads"]
