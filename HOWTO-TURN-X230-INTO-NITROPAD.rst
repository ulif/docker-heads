How to Turn a Lenovo x230 into a Nitropad
*****************************************

Extended version of https://www.reddit.com/r/Qubes/comments/fg5rjg/lenovo_x230_coreboot_heads/

1) Buy Hardware
===============

Good quality x230 + LibremKey/Nitrokey + USB + anything you
need to upgrade the x230 (classic keyboard, IPS screen, new wifi card, SSD,
mSata SSD, plugs, batteries, anything needing repliacing, fan being a big
possible one)

What we used:

- Lenovo x230
- Raspberry PI 3 B+ (with a proper power supply and a 16 GB SD card)
- USB pendrive for storing GPG offline key
- USB pendrive with Debian Installer
- A 2nd laptop/PC to build the local docker image
- 6+ female2female jumper cables
- SOIC-8-Clip
- Nitrokey Pro 2
- Smartphone (for TOTP)
- Ethernet cable to provide network for the raspi and the x230
- Internet Access




2) Update the BIOS
==================

Update it to 2.76, EC 1.14 and *no higher as otherwise you can't do step 3*

Get the ISO-image list from https://support.lenovo.com/at/en/downloads/DS029187
where you can pick the suitable version according to
https://github.com/hamishcoleman/thinkpad-ec.

At time of writing, we used 2.76 with EC 1.14, available at

    https://download.lenovo.com/pccbbs/mobiles/g2uj32us.iso

Download the ISO-image, burn it on a CD and boot from it. It is only a few
dozen MiB in size,It is only a few dozen MiB in size but does not work from a
USB pendrive.


2.1) Roll back to an unlocked BIOS version
------------------------------------------

If the BIOS/EC version of your x230 is too high (BIOS > 2.76, EC > 1.14), then
you might be able to roll back to a supported version for step 3.

This is only possible if in your BIOS the Option ``Security | UEFI BIOS Update
Option | Secure Rollback Prevention`` is set to ``Disabled``.

If this is not the case,  then you are probably out of luck and have to live
with BIOS internal whitelisting. Skip step 3 and proceed with step 4.

Otherwise, you can download a suitable version (again refer to the table on
https://github.com/hamishcoleman/thinkpad-ec) from
https://support.lenovo.com/at/en/downloads/DS029187, but this time you cannot
use ISO files but have to pick the ``.exe`` files instead. That is because the
flash utility in the ISO image does not continue to flash, when it detects a
more recent version in the BIOS. Therefore you have to run the downloadable
executable.

Even worse: you need a full Windows running on your x230 for starting the flash
utility. A DOS environment (as FreeDOS) is *not* sufficient, although the main
flashing process after installing the utility happens in a plain DOS box.

We unfortunately updated to the most recent version and had to roll back. We
grabbed

   https://download.lenovo.com/pccbbs/mobiles/g2uj32us.exe

started that in a freshly installed, fully fledged Windows 7 and were able to
roll back the BIOS and EC version.

[Discussion about downgrading: https://forum.thinkpads.com/viewtopic.php?t=129745]


3) Unlock Whitelists
====================

Use Hamish Coleman's patch to allow you to use aftermarket batteries and (if
you want) use the classic keyboard. You can't do this after corebooting

Follow https://github.com/hamishcoleman/thinkpad-ec

You can build our docker container that will create a suitable image (3.1)
or do every step yourself (3.2).


3.1) Build patched.x230.img in Docker
-------------------------------------

After installing docker, grab the local Dockerfile and run

    docker build -t heads .

That will take a long time (depending on your machine: 45 to 90 minutes!) but
will create the most needed flash images and tools for you. Also the tools
needed in the upcoming steps.

One of these will be the patched EC to remove whitelisting from the BIOS/EC.
The generated image will contain both, patches for unblocking classic keyboards
and aftermarket batteries.

To copy the results over in the local dir:

    docker run --rm -v `pwd`/.:/data heads cp -r /build/results /data

gives you all relevant files in ``results/``.


3.2) Build patched.x230.img yourself
------------------------------------

If instead of letting docker do the work for you, you prefer to do it yourself,
then follow the recipe given in the link above or have a look at the steps
taken in the Dockerfile.


3.3) Write generated patched.x230.img on a USB stick and flash
--------------------------------------------------------------

Insert your USB pendrive and run

    lsblk -d -o NAME,SIZE,LABEL

to identify the device number of your pendrive. Then run

    sudo dd if=patched.x230.img of=/dev/sdX bs=4M status=progress conv=fsync

with ``sdX`` being your USB stick identified before. Please note that *ALL DATA
will be LOST* on the stick.

Finally insert the stick into your x230 and boot it. Flash the firmware. This
is describen in detail at
https://github.com/hamishcoleman/thinkpad-ec#booting-the-stick-and-flashing-the-firmware.


4) Install a Linux distro
==========================

onto your x230. Personally I'd just use something simple like Debian, rather
than Qubes at this stage, but no reason you can't do it from Qubes. You might
prefer something simple to install, as we will install the actual system in a
later step.

Do not use full-disk encryption, as heads might stumble over that later-on.

*Note for Debian Users (also derivates)*: Debian installer images will not work
properly when run in a later stage (after heads install). They do not cope with
the changed VGA-ROM.  The screen will remain black.

We still have not found a workaround, except installing on a different machine
and then moving the harddrive/SSD. This is true also for some Debian derivates
like Kali. The main exception we found, was Ubuntu (20.04). The Ubuntu
installer worked fine out of the box also on the finally flashed heads.

Another workaround is using the Debian live CD/DVD which works flawlessly, but
comes with limited installation options (no LVM, only limited disk encryption
possibilities).

If you want to use a stock Debian install medium for installing Debian, now is
a good time to do it.

Whatever you install now, keep `/boot` unencrypted as heads will try to scan
the directory and hash the contents.



5) Setup Nitrokey/LibremKey
===========================

Libremkeys are rebranded Nitrokeys. We use the [Nitrokey Pro 2](https://www.nitrokey.com/#comparison).

Read

  https://docs.puri.sm/Librem_Key/Getting_Started/User_Manual.html

to learn the details. This manual is easier to follow than the Nitrokey docs.

  https://www.nitrokey.com/documentation/applications

We started by booting from a [TAILS](https://tails.boum.org/) system, w/o
network but with root password set and inserted the NitroKey.


5.1) Set Nitrokey Passwords
---------------------------

Then, in a Terminal::

    gpg --card-status

should give some (more or less cryptic) infos about the NitroKey.

::
    gpg --card-edit
    admin
    passwd
    1
    <ENTER OLD USER PIN - Default: 123456>
    <ENTER NEW PIN>
    <ENTER NEW PIN AGAIN>
    3
    <ENTER OLD ADMIN PIN - Default: 12345678>
    <ENTER NEW ADMIN PIN>
    <ENTER NEW ADMIN PIN>
    q
    q


5.2) Create a GPG key and store it offline
-------------------------------------------

We generate the Key on the TAILS machine to have a possibility available to
restore the key in case of emergency.

Because TAIL live systems come prepopulated with GPG keys, we create a new home
for the new keyring and set the location via env var::

    mkdir mynewkeys
    chmod go-rwx mynewkeys
    export GNUPGHOME=~/mynewkeys

Check that this worked by listing the keyring contents::

    gpg -k

should give a message about a freshly created keyring and show no keys (yet).
Staying in the shell we finally create our main key::

    gpg --full-gen-key
    Kind of Key: 1     # RSA and RSA
    Keysize: 4096
    Valid for: 2y (two years)
    Real Name: not your real name
    Email Address: only a real email address if you really want to use it
    Comment: does not matter

Edit the key dropping to the GPG-shell::

    gpg --expert --edit-key <EMAIL-OF-YOUR-KEY>

In the gpg-shell, create missing subkeys. We will need three of them, one for (E)ncryption, one for ((S)igning and one for (A)uthentication. Normally, this can be done by the following steps::

    addkey
    RSA (sign only)
    4096
    2y

    addkey
    RSA (set your own capabilitites)
    A
    S
    E
    Q
    4096
    2y


Now set the trust level to ultimate::

    trust
    5 (ultimate)
    save

Create a revocation cert::

    gpg --output revoke.asc --gen-revoke <youremail@yourdomain.com

Move it over to your temporary GnuPG home::

    mv revoke.asc mykeys/

Back up the whole data to a USB thumb drive::

    cp -r ~/mykeys /media/amnesia/my-usb/_gnupg

To make a regular USB pendrive accessible in `tails`, just plug it in, open a file manager
and click on the respective drive name.

You might also want to create export files of your keys to back them up::

    gpg --armor --output privkey.sec --export-secret-key <youremail@yourdomain.com>
    gpg --armor --output subkey.sec --export-secret-subkeys <youremail@yourdomain.com>
    gpg --armor --output pubkey.asc --export <youremail@yourdomain.com>

Also put the keys on your USB pendrive. The last one is especially important,
as `heads` will ask for it to sign everything later on.

::
    cp privkey.sec subkey.sec pubkey.asc /media/amnesia/my-usb

Remove the pendrive properly - we will reuse it later. The key stored on it
should be kept in a really secure location.


5.3) Copy GPG Key Over to the Nitrokey
--------------------------------------

Ensure, the Nitrokey is still accessible by running::

    gpg --card-status

Should give no error. Now, edit the key again::

    gpg --expert --edit-key <youremail@yourdomain.com>

We will now select each of the subkeys separately and one after the other to
transfer it to the Nitrokey. We start with the first subkey, by typing::

    key 1
    keytocard

Now deselect key 1, and select key 2::

    key 1
    key 2
    keytocard

The same for the last subkey::

    key 2
    key 3
    keytocard

and at the end type::

    save

to store everything to the Nitrokey and end the session.


5.4) How to Sign Boot Images
----------------------------

Oh, and a GPG-related hint for signing .iso images on USBs:

    gpg -b -a MYIMAGE.iso

will create a MYIMAGE.iso.asc

Do it with your new key as only one in the keyring, and you can use these
detached signatures for booting from the respective images. heads requires such
signatures, if you want to boot from USB.


6) Install Coreboot (with Skulls)
=================================

cf. https://github.com/merge/skulls/tree/master/x230


6.1) Prepare a raspberry pi
---------------------------

We used a raspberry pi (3b+) to flash the x230. You can also use dedicated
programmers like the ch314a, but this is out of the scope of our little
tutorial.

The raspberry pi should be installed with a recent raspbian and you should be
able to connect to it via network, keyboard, or serial cable. Network access is
essential to install missing tools.

Login to your raspi and install `flashrom`::

    (pi@pi) $ sudo apt install flashrom

Furthermore make sure, the needed modules are active::

    (pi@pi) $ sudo modprobe spi_bcm2708
    (pi@pi) $ sudo modprobe spidev

and useful bootparams are enabled. In `/boot/config.txt` set `enable_uart=1`
and `dtparam=spi=on`

Optional: prepare `skulls` install if you do not want to configure/compile
coreboot yourself. To get `skulls` get the latest version from
https://github.com/merge/skulls/releases ::

    (pi@pi) $ wget https://github.com/merge/skulls/releases/download/0.1.9/skulls-x230-0.1.9.tar.xz
    (pi@pi) $ wget https://github.com/merge/skulls/releases/download/0.1.9/skulls-x230-0.1.9.tar.xz.sha512

Compare the hash sums::

    (pi@pi) $ cat *.sha512 && sha512sum skulls-x230-0.1.9.tar.xz

and ensure they match.


6.2) Open the x230 and reveal the chips to flash
------------------------------------------------

Start by removing the battery and then removing the seven screws (X) as shown
here.

Bottom of the ThinkPad -- 'X' marks screws to be removed ('o' remain)::

    +--------------------------------------------------------+
    | #|        Battery Area                |              # |
    | #+------------------------------------+              # |
    |o                                                       |
    ||                                                       |
    ||              +---X-----------------+                  |
    ||              |o                   o|                  |
    ||              |     /        /      |               o  |
    |+              |     /        /      |                  |
    |  X /          |     /        /      |           -- X   |
    |    /          |     /  X     /      |   //////////     |
    |    /          |     /        /      |   //////////     |
    |  # / --       +---------------------+               #  |
    |  # /X                                               #  |
    |               X                     X                  |
    +--------------------------------------------------------+

or instance following the image on

https://www.chucknemeth.com/laptop/lenovo-x230/flash-lenovo-x230-coreboot

Slide the keyboard toward the screen until you can lift it out of the way. Be
careful as it is still attached to the motherboard.

The small ribbon cable connecting the keyboard can then be removed by gently
pulling it off.

After that you want to remov the palm rest. Also act gently here and decide,
whether you want the cable connecting it, attached or removed.

Finally you can reveal the two BIOS chips we are interested by carefully
peeling the protective film in the lower left back. You might want to tape the
film to stay out of the way.


6.3) Connect the clip with the raspberry pi
-------------------------------------------

X230 BIOS Chip pinout::

   Screen (furthest from you)
               ___
    MOSI  5 --|   |-- 4  GND
     CLK  6 --|   |-- 3  N/C
     N/C  7 --|   |-- 2  MISO
     VCC  8 --|__o|-- 1  CS

     Edge (closest to you)


    Edge of pi (furthest from you)
                                                       1
    L                                                  CS
    E                                                  |
    F +------------------------------------------------------------------------------------+
    T |    x   x   x   x   x   x   x   x   x   x   x   x   x   x   x   x   x   x   x   x   |
      |    x   x   x   x   x   x   x   x   x   x   x   x   x   x   x   x   x   x   x   x   |
    E +------------------------------------^---^---^---^-------------------------------^---+
    D                                      |   |   |   |                               |
    G                                   3.3V MOSI MISO |                              GND
    E                                  (VCC)          CLK
                                           8   5   2   6                               4
     Body of Pi (closest to you)


6.4) Backup old BIOS
--------------------

Use the clip to connect the raspberrypi with the top chip (the one next to
the screen).

See, if the connection works::

    (pi@pi) $ sudo flashrom -p linux_spi:dev=/dev/spidev0.0,spispeed=512

You might have to remove the speedsetting at the end of the command. If,
amongst other things, you get a message ``No EEPROM/flash device found``, check
cables and the connection and try again, maybe skipping the speed setting.

If a chip type could be identified, read the ROM at least two times::

    (pi@pi) $ sudo flashrom -p linux_spi:dev=/dev/spidev0.0 -r top_rom1.bin
    (pi@pi) $ sudo flashrom -p linux_spi:dev=/dev/spidev0.0 -r top_rom2.bin

Do a third run or compare the results, to ensure reading worked correctly::

    (pi@pi) $ diff top_rom1.bin top_rom2.bin

No output means no problem in that case.

If done, remove the clip from the upper chip, connect it to the bottom one and
repeat the procedure with different image names::

    (pi@pi) $ sudo flashrom -p linux_spi:dev=/dev/spidev0.0 -r bottom_rom1.bin
    (pi@pi) $ sudo flashrom -p linux_spi:dev=/dev/spidev0.0 -r bottom_rom2.bin

Do a third run or compare the results, to ensure reading worked correctly::

    (pi@pi) $ diff bottom_rom1.bin lower_rom2.bin

Afterwards copy top and bottom chip images to a safe place. They will be the
only possibility to restore the state as it was before flashing.


6.5) Flash coreboot via skulls
------------------------------

We start with the bottom chip::

    (pi@pi) $ tar -xf skulls-x230-0.1.9.tar.xz
    (pi@pi) $ cd skulls-x230-0.1.9
    (pi@pi) $ sudo external_install_bottom.sh -m -k bottom.rom

`-m` means to nuke the ME and `-k` gives another backup file. Similarily for
the other chip::

    (pi@pi) $ sudo external_install_top.sh -m -k top.rom

Make sure to store top.rom and bottom.rom in a safe location. For instance on
the USB pendrive holding your GPG keys.

7) Flash heads
==============

8) Setup heads
==============

If everything worked, after a reboot you should be presented a graphical menu
asking you whether to perform an OEM Factory Reset? Select `Cancel`, if you
have already a GPG key (as shown above).

If instead you do *not* want a backup of your GPG key, you can select
`Continue`. This will create a new GPG key on your Nitrokey (but the secret key
will stay on the Nitrokey).

Here, we follow track 1 and will cancel, to use our own key.

Add your generated key by selecting

    `Add a GPG key to the running BIOS`

then

    `Add GPG key to running BIOS + reflash`

Plug in the USB pendrive prepared above with your GPG key and select

    `Yes`

to proceed. You USB device will be scanned and GPG keys found presented. Select
the public key you exoorted above (and hopefully named `pubkey.asc`).

    `/media/pubkey.asc`

Asked, whether the BIOS should be flashed, answer

    `Yes`

If everything went well, you will be asked to sign all files in the `/boot`
directory of your OS. If you installed a proper OS with a readable
(non-encrypted) and separate (on an own partition) `/boot` partition, say

    `Yes`

here.

Next you will be asked, whether you want to intialize/update the TPM counter
and set a password. Do that. If doing it for the first time, the operation
will throw some error messages. Do not worry. In the end you should see a
screen telling, that checksums were updated and files in /boot signed.

    `REBOOT`

After reboot, heads will most probably complain that it could not generate a
TOTP code. No wonder, as we did not create any HOTP/TOTP secret yet. Do it now:

    `Generate new HOTP/TOTP secret`
    `Yes`

If that fails, try to

    `Reset the TPM`

first and generate a new HOTP/TOTP secret. In case of success the secret will
be output on the screen as a QR code you can scan with your smartphone with an
app like, for instance, the android app `andOTP` which is also available in
`fdroid` app store.

All of the steps above can be repeated, if neccessary. For instance checksums
have to be updated, after kernel updates.
